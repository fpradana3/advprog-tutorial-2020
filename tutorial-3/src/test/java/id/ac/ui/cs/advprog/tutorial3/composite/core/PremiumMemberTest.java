package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member child = new OrdinaryMember("Sasuke", "Uchiha");
        member.addChildMember(child);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member child = new OrdinaryMember("Sasuke", "Uchiha");
        member.addChildMember(child);
        assertEquals(1, member.getChildMembers().size());
        member.removeChildMember(child);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member first = new OrdinaryMember("first", "member");
        member.addChildMember(first);
        Member second = new OrdinaryMember("second", "member");
        member.addChildMember(second);
        Member third = new OrdinaryMember("third", "member");
        member.addChildMember(third);
        Member fourth = new OrdinaryMember("fourth", "member");
        member.addChildMember(fourth);
        assertEquals(3, member.getChildMembers().size());

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Guild Master", "Master");
        Member first = new OrdinaryMember("first", "member");
        master.addChildMember(first);
        Member second = new OrdinaryMember("second", "member");
        master.addChildMember(second);
        Member third = new OrdinaryMember("third", "member");
        master.addChildMember(third);
        Member fourth = new OrdinaryMember("fourth", "member");
        master.addChildMember(fourth);
        assertEquals(4, master.getChildMembers().size());
    }
}
