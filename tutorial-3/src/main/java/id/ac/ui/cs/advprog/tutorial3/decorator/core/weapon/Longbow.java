package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        //TODO: Complete me
        public Longbow() {
            this.weaponName = "Longbow";
            this.weaponDescription = "Big Longbow";
            this.weaponValue = 15;
        }
}
