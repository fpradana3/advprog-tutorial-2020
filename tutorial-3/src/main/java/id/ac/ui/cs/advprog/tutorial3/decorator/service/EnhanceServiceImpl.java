package id.ac.ui.cs.advprog.tutorial3.decorator.service;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.repository.EnhanceRepository;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

@Service
public class EnhanceServiceImpl implements EnhanceService {

    private ArrayList<Weapon> weapons;
    private EnhanceRepository enhanceRepository;

    public EnhanceServiceImpl(ArrayList<Weapon> weapons, EnhanceRepository enhanceRepository){
        this.weapons = weapons;
        this.enhanceRepository = enhanceRepository;
        weaponInit();
    }

    public void weaponInit() {
        Weapon gun = new Gun();
        Weapon longbow = new Longbow();
        Weapon shield = new Shield();
        Weapon sword = new Sword();
        weapons.add(gun);
        weapons.add(longbow);
        weapons.add(shield);
        weapons.add(sword);
    }

    @Override
    public void enhanceAllWeapons() {
        enhanceRepository.enhanceToAllWeapons(weapons);
    }

    @Override
    public Iterable<Weapon> getAllWeapons() {
        return weapons;
    }
}
